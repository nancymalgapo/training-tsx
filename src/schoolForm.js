import React from 'react'

class SchoolInfo extends React.Component {

    schoolData;

    constructor(props) {
        super(props)
        this.state = {
            schoolName: null,
            gradeLevel: null,
            studentNumber: null,
            classNumber: null,
            isAirconditioned: true,
            alertValue: ""
        };

        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

    validateInputs = () => {
        const inputs = [this.state.schoolName, this.state.gradeLevel, 
        this.state.studentNumber, this.state.classNumber]
        inputs.map(input => {
            if (!input) {
                this.setState({
                    alertValue: "Please fill-up the required fields."
                })
            } else {
                this.setState({
                    alertValue: "Form is submitted."
                })
            }
        })
    }

    handleOnSubmit = (event) => {
        alert(this.state.alertValue);
        event.preventDefault();
        localStorage.setItem('document', JSON.stringify(this.state));
    }

    componentDidMount() {
        this.schoolData = JSON.parse(localStorage.getItem('document'));

        if (localStorage.getItem('document')) {
            this.setState({
                schoolName: this.state.schoolName,
                gradeLevel: this.state.gradeLevel,
                studentNumber: this.state.studentNumber,
                classNumber: this.state.classNumber,
                isAirconditioned: this.state.isAirconditioned
            })
        } else {
            this.setState({
                schoolName: "",
                gradeLevel: "",
                studentNumber: "",
                classNumber: "",
                isAirconditioned: false
            })
        }
    }

    render() {
        var message;
        if (this.state.isAirconditioned) {
            message = "Airconditioned"
        } else {
            message = "Not Airconditioned"
        }
        return (
            <div>
                <form onSubmit={this.handleOnSubmit}>
                    <h1> School Information </h1>
                    <h3> School Name: <input name="schoolName" type="text" value={this.state.schoolName} onChange={this.handleInputChange} /></h3>
                    <h3> Grade Level: <input name="gradeLevel" type="number" min="1" max="12" value={this.state.gradeLevel} onChange={this.handleInputChange} /></h3>
                    <h3> Number of Students: <input name="studentNumber" type="number" min="1" max="50" value={this.state.studentNumber} onChange={this.handleInputChange} /></h3>
                    <h3> Number of Classes: <input name="classNumber" type="number" min="1" max="10" value={this.state.classNumber} onChange={this.handleInputChange} /></h3>
                    <h3> Room Type: <br />
                        <label>
                            <input
                                name="isAirconditioned"
                                type="checkbox"
                                defaultChecked={this.state.isAirconditioned}
                                onChange={this.handleInputChange} />
                            {message}
                        </label>
                    </h3>
                    <input type="submit" name="submitButton" onClick={this.validateInputs} /> 
                </form>
            </div>
        );
    }
}

export default SchoolInfo;
