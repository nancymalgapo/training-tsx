import React from 'react';

class Clock extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      date: new Date(),
      randomNumber: 0
    };
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      date: new Date(),
      randomNumber: Math.floor(Math.random()*1000)
    });
  }

  render() {
    return (
      <div>
        <h1> It is {this.state.date.toLocaleTimeString()}.</h1>
        <h2> Random Number: {this.state.randomNumber} </h2>
      </div>
    );
  }
}

export default Clock;
