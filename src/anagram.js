import React from 'react'

class Anagram extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            input1: "",
            input2: "",
            decision: ""
        }
    }

    handleInput1(event) {
        this.setState({
            input1: event.target.value,
        })
    }

    handleInput2(event) {
        this.setState({
            input2: event.target.value,
        })
    }

    feedHandler(event) {
        this.setState({
            decision: this.isAnagram(this.state.input1, this.state.input2)
        })
    }

    isAnagram(input1, input2) {
        input1 = input1.toLowerCase()
        input2 = input2.toLowerCase()

        if (input1.split('').sort().join('') === input2.split('').sort().join('')) {
            return "It's an Anagram";
        } else {
            return "Not an Anagram";
        }
    }

    render() {
        let word1 = this.state.input1;
        let word2 = this.state.input2;
        let answer = this.state.decision;
        return (
            <div>
                <form>
                    <h2> Input 1: <input type="text" name="input1" value={word1} onInput={this.handleInput1.bind(this)} /> </h2>
                    <h2> Input 2: <input type="text" name="input2" value={word2} onInput={this.handleInput2.bind(this)} /> </h2>
                    <h1> Anagram: {answer} </h1>
                    <button onClick={ (event) => this.feedHandler(event) }> Click Here </button>
                </form>
            </div>
        );
    }
}

export default Anagram;