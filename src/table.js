import React from 'react'

class EventHandler extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            firstName: "",
            lastName: "",
            age: 0,
            id: 0,
        }
    }

    handleFeed(event) {
        this.setState({
            firstName: 'Nancy',
            lastName: 'Malgapo',
            age: '24',
            id: 123
        })
    }

    render() {
        let names = [
            {
                firstName: 'Nancy',
                lastName: 'Malgapo',
                age: '24'
            }
        ]
        let numbers = [1, 2, 3]
        const listItems = numbers.map((number) => <li> {number} </li> );
        return (
            <div>
                <ul>
                    {listItems}
                </ul>
                <table>
                    <tr>
                        <td> {this.state.firstName} </td>
                        <td> {this.state.lastName} </td>
                        <td> {this.state.age} </td>
                    </tr>
                </table>
                <button onClick={ (event) => this.handleFeed(event) }> Click Here </button>
            </div>
        );
    }
}

export default EventHandler;
