import React from 'react'
import {BrowserRouter, Route, Switch} from 'react-router-dom'


class Home extends React.Component {
  render() {
    return <div>
      <h4>Home</h4>
      <p>This is Home page.</p>
    </div>
  }
}
 
export default Home;
