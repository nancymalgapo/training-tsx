import React from 'react';
import { BrowserRouter, Switch, Route, Link} from 'react-router-dom'
import Contact from './contact'
import Home from './home';
import About from './about';
import Record from './record'

class App extends React.Component {
    constructor(props) {
      super(props)
    }

    render() {
      return(
        <div>
          <BrowserRouter>
            <div>
              <h3>Sample App Routing</h3>
              <ul>
                <li> <Link to="/">Home</Link> </li>
                <li> <Link to="/about">About Us</Link> </li>
                <li> <Link to="/contact">Contact</Link> </li>
                <li> <Link to="/create_record">Create Record</Link> </li>
              </ul>
              <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/about" component={About} />
                <Route path="/contact" component={Contact} />
                <Route path="/create_record" component={Record} />
              </Switch>
            </div>
          </BrowserRouter>
        </div>
      );
    }
}
 
export default App;
