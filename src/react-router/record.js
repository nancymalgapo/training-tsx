import React from 'react';
import { BrowserRouter, Switch, Route, Link, Redirect } from 'react-router-dom'
 
class Record extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            step: 1
        }
    }

    onButtonClick(event) {
        let counter = this.state.step + 1;
        if (counter <= 3) {
            this.setState({
                step: counter
            })
        }
        let url = `/create_record/step=${this.state.step}/`;
        this.props.history.push(url)
    }

    render() {
        return (
            <div>
                <h4>Create Record</h4>
                <p>Create a Record by finishing all the steps.</p>
                <button onClick={ (event) => this.onButtonClick(event) }> Go to Step {this.state.step} </button>
            </div>
        )
    }
}
 
export default Record;
