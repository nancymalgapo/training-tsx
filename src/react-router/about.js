import React from 'react'
import {BrowserRouter, Route, Switch} from 'react-router-dom'


class About extends React.Component {
  render() {
    return <div>
      <h4>About</h4>
      <p>This is About page.</p>
    </div>
  }
}
 
export default About;