import React from 'react'

class Palindrome extends React.Component {

    isPalindrome(word) {
        var str_input = word.toLowerCase();
        var word_len =  str_input.length;
        for (var i=0; i<word_len/2; i++) {
            if (str_input[i] !== str_input[word_len-1-i]) {
                return "False";
            }
        }
        return "True";
    }

    render() {
        let word = this.props.word;
        let decision = this.isPalindrome(word);
        return (
            <div>
                <h1> Word: {word} </h1>
                <h2> Palindrome: {decision} </h2>
            </div>
        );
    }
}

export default Palindrome;
