import React from 'react'
 
class Finder extends React.Component{
    constructor(props) {
        super(props)
    }

    handleFullNameChange = (event) => {
        this.props.onFullnameChange(event.target.value)
    }

    handleAgeChange = (event) => {
        this.props.onAgeChange(event.target.value)
    }

    handleAirconChange = (event) => {
        this.props.onAirconChange(event.target.checked)
    }

    handleParentControlChange = (event) => {
        this.props.onParentControlChange(event.target.checked)
    }

    render() {
        return (
            <div>
                Full Name: <input value={this.props.info.fullName} onChange={this.handleFullNameChange}/> <br/>
                Age: <input value={this.props.info.age} onChange={this.handleAgeChange}/> <br/>
                <input type="checkbox" value={this.props.info.needsAircon} onChange={this.handleAirconChange}/> Is Airconditioned <br/>
                <input type="checkbox" value={this.props.info.parentControl} onChange={this.handleParentControlChange}/> Needs Parental Control <br/>
            </div>
        )
    }
}

export default Finder;
