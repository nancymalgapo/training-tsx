import React from 'react'
import Finder from './finder'
import Result from './results'
 
const schools = [
    {
        schoolName: "Ateneo",
        maxAge: 7,
        isAiconditioned: true,
        parentControl: true
    },
    {
        schoolName: "La Salle",
        maxAge: 9,
        isAiconditioned: true,
        parentControl: true
    },
    {
        schoolName: "Poveda",
        maxAge: 15,
        isAiconditioned: true,
        parentControl: false
    },
    {
        schoolName: "PUP",
        maxAge: 18,
        isAiconditioned: false,
        parentControl: false
    },
]

class Registration extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            age: 0,
            needsAircon: true,
            fullName: 'Nancy Malgapo',
            parentControl: false
        }
    }

    handleFullNameChange = (e) => {
        this.setState({fullName: e}) 
    }

    handleAgeChange = (e) => {
        this.setState({age: e}) 
    }

    handleAirconChange = (e) => {
        this.setState({needsAircon: e}) 
    }

    handleParentControlChange = (e) => {
        this.setState({parentControl: e})
    }


    filterSchools = () => {
        return schools.filter(e => {
            return (this.state.age<e.maxAge &&
            this.state.needsAircon == e.needsAircon &&
            this.state.parentControl == e.parentControl)
        })
    }

    render(){
        return (
            <div>
                <Finder 
                    onFullnameChange={this.handleFullNameChange}
                    onAgeChange={this.handleAgeChange}
                    onAirconChange={this.handleAirconChange}
                    onParentControlChange={this.handleParentControlChange}
                    info={this.state} />
                <Result schools={this.filterSchools()}/>
            </div>
        )
    }
}

export default Registration;
