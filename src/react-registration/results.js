import React from 'react'

class Result extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        const allSchools = this.props.schools.map(entry =>
        <div> 
            School: {entry.schoolName} <br/>
            Max Age: {entry.maxAge} <br/>
            Aircon Availability: {entry.aircon.toString()} <br/>
            Parental Control: {entry.parentControl.toString()} <br/>
        </div>
        )
        return (
            <div>
                {allSchools ? allSchools : []}
            </div>
        )
    }
}

export default Result;
