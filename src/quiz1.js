import React from 'react';

class Quiz1 extends React.Component {

    ageDefiner(age) {
        if (age == 0) {
            return "Baby";
        }
        else if (age >= 1 && age <= 3) {
            return "Toddler";
        }
        else if (age > 3 && age <= 5) {
            return "Preschooler";
        }
        else if (age > 5 && age <= 12) {
            return "Gradeschooler";
        }
        else if (age > 12 && age <= 18) {
            return "Teenager";
        }
        else if (age > 18 && age <= 21) {
            return "Young Adult";
        }
    }

    render() {
        let age = this.props.age;
        let description = this.ageDefiner(age);
        return <h1> Hello, your age is {age}. You are a {description}. </h1>
    }
}

export default Quiz1;
