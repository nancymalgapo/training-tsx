import React from 'react'

class LoginForm extends React.Component {

    loginData;

    constructor(props) {
        super(props)
        this.state = {
            username: "",
            password: "",
            alertValue: ""
        };
        this.handleUsername = this.handleUsername.bind(this);
        this.handlePassword = this.handlePassword.bind(this);
    }

    handleUsername(event) {
        this.setState({
            username: event.target.value
        })
    }

    handlePassword(event) {
        this.setState({
            password: event.target.value
        })
    }

   validateInputs = () => {
        const inputs = [this.state.username, this.state.password]
        inputs.map(input => {
            if (!input) {
                this.setState({
                    alertValue: "Please fill-up the required fields."
                })
            } else {
                this.setState({
                    alertValue: "Form is submitted."
                })
            }
        })
    }

    handleOnSubmit = (event) => {
        alert(this.state.alertValue)
        event.preventDefault();
        localStorage.setItem('login', JSON.stringify(this.state));
    }

    componentDidMount() {
        this.loginData = JSON.parse(localStorage.getItem('login'));

        if (localStorage.getItem('login')) {
            this.setState({
                username: this.state.username,
                password: this.state.password
            })
        } else {
            this.setState({
                username: "",
                password: ""
            })
        }
    }

    componentWillUpdate(nextProps, nextState) {
        localStorage.setItem('login', JSON.stringify(nextState));
    }

    render() {
        return(
            <div>
                <form onSubmit={this.handleOnSubmit}>
                    <h1> Login </h1>
                    <h3>Username: <input type="text" onChange={this.handleUsername} /> </h3>
                    <h3>Password: <input type="password" onChange={this.handlePassword} /> </h3>
                    <input type="submit" name="submitButton" onClick={this.validateInputs} />
                </form>
            </div>
        );
    }
}

export default LoginForm;
