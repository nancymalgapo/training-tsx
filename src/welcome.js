import React from 'react';
import SimpleBorder from './SimpleBorder'

class Hello extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            counter: 0
        }
    }
    componentDidMount() {
        console.log('Component Mounted Now')
        this.setState({
            counter: 1,
        })
    }
    componentWillMount() {
        console.log('Component Mounted Now')
        this.setState({
            counter: 14,
        })
    }
    render(){
        return (
            <div>
                <h1>Counter: {this.state.counter} </h1>
                <SimpleBorder />
            </div>
        );
    }

}

export default Hello;
